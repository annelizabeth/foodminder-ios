//
//  FoodType.swift
//  FoodMinder
//
//  Created by Ann-Elizabeth Salazar on 4/13/15.
//  Copyright (c) 2015 Elizabeth Salazar. All rights reserved.
//

import Foundation

enum FoodType: Int {
    case Dairy = 0, Meat, Vegetables, Fruit, Grain, Beverage, Frozen, Other
    
    static var count: Int { return FoodType.Other.hashValue + 1}
    
    static var nameStrings: [String] {
        return ["Dairy", "Meat", "Vegetables", "Fruit", "Grain", "Beverage", "Frozen", "Other"]
    }
}
