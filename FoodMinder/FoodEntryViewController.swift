//
//  FoodEntryViewController.swift
//  FoodMinder
//
//  Created by Ann-Elizabeth Salazar on 4/12/15.
//  Copyright (c) 2015 Elizabeth Salazar. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class FoodEntryViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var foodField: UITextField!
    @IBOutlet weak var daysField: UITextField!
    @IBOutlet weak var foodTypePicker: UIPickerView!
    @IBOutlet weak var chooseTypeButton: UIButton!
    
    var objectContext: NSManagedObjectContext = NSManagedObjectContext()
    
    var foodCategory: FoodType = FoodType(rawValue: 0)!
    
    override func viewDidAppear(animated: Bool) {
        foodTypePicker.hidden = true
        foodTypePicker.delegate = self
        foodTypePicker.backgroundColor = UIColor.whiteColor()
        
    }
    
    // MARK: Helper Methods
    
    func daysToDate() -> NSDate {
        
        let now = NSDate()
        let daysToExpire = daysField.text.toInt()
        return NSCalendar.currentCalendar().dateByAddingUnit(
            .CalendarUnitDay,
            value: daysToExpire!,
            toDate: now,
            options: NSCalendarOptions(0))!
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toFoodTable" {
            
        }
    }
    
    // MARK: IBAction Methods
    
    @IBAction func chooseFoodTypePressed(sender: UIButton) {
        
        foodTypePicker.hidden = false
        
    }
    
    @IBAction func submitPressed(sender: UIButton) {
        var foodName = ""
        foodName = foodField.text
        var foodExpiry = NSDate()
        foodExpiry = daysToDate()
        
        var enteredFood = Food(name: foodName, expiryDate: foodExpiry, category: foodCategory)
        
        //Save this food in the model
    }
    
}

    extension FoodEntryViewController: UIPickerViewDataSource {
        
        func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return FoodType.count
        }
    }
    
    extension FoodEntryViewController: UIPickerViewDelegate {
        
        func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
            return chooseTypeButton.frame.size.width
        }
        
        func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
            return chooseTypeButton.frame.size.height
        }
        
        func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
            return FoodType.nameStrings[row]
        }
        
        func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            foodCategory = FoodType(rawValue: row)!
            chooseTypeButton.setTitle(FoodType.nameStrings[row], forState: UIControlState.Normal)
            pickerView.hidden = true
        }
    }
