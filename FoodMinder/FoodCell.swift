//
//  FoodCell.swift
//  FoodMinder
//
//  Created by Ann-Elizabeth Salazar on 4/12/15.
//  Copyright (c) 2015 Elizabeth Salazar. All rights reserved.
//

import Foundation
import UIKit

class FoodCell: UITableViewCell {
    
    var food: Food?
    
    @IBOutlet weak var hadSinceLabel: UILabel!
    @IBOutlet weak var eatByLabel: UILabel!
    @IBOutlet weak var foodImage: UIImageView!
    @IBOutlet weak var foodNameLabel: UILabel!
    

    func configureCellWithFood(food: Food) {
        
    }

    @IBAction func viewCalendarButton(sender: UIButton) {
    }
    
}