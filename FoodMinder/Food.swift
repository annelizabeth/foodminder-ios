//
//  Food.swift
//  FoodMinder
//
//  Created by Ann-Elizabeth Salazar on 4/13/15.
//  Copyright (c) 2015 Elizabeth Salazar. All rights reserved.
//

import Foundation
import CoreData

class Food: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var expiration: NSDate
    @NSManaged var category: NSNumber

}
